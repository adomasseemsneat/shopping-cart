import React, { useState, useEffect } from 'react';
import productsSlice, { Product } from './products.slice';


const ProductForm: React.FC = () => {


  const initialState = {
    title: "",
    price: 0,
    id: ''
  }

  const [{title, price, id}, setProduct] = useState<Product>(initialState)


  const handleChange = ({  target: {name, value}  }: 
    React.ChangeEvent<HTMLInputElement>) => setProduct(prev => {
      (prev as any)[name] = value;
      const newValue = {...prev} 
      return newValue;
    })


  const handleSubmit = () => {
    alert('submit')
  }

  return (
    <>
      <h2>add game to store</h2>
      <form onSubmit={handleSubmit} action="">
        <input 
          type="text" placeholder="Game Title" name="title" 
          value={title} onChange={handleChange}
        />
        <input 
          type="number" placeholder="Game Price" name="Price" 
          value={price} onChange={handleChange}
        />
        <input 
          type="text" placeholder="Game id" name="id"
          value={id} onChange={handleChange}
        />
        <button type="submit">Add game</button>
      </form>
    </>
  );
};

export default ProductForm;